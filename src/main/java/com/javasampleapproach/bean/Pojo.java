package com.javasampleapproach.bean;

public class Pojo {
	
	private String name;
	private String gender;
	private long count;
	public String getName() {
		return name;
	}
	@Override
	public String toString() {
		return "Pojo [name=" + name + ", gender=" + gender + ", count=" + count + "]";
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public long getCount() {
		return count;
	}
	public void setCount(long l) {
		this.count = l;
	}
}
