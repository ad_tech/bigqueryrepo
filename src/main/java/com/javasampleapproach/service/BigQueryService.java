package com.javasampleapproach.service;

import java.util.ArrayList;
import java.util.List;
import com.google.cloud.bigquery.BigQuery;
import com.google.cloud.bigquery.BigQueryException;
import com.google.cloud.bigquery.BigQueryOptions;
import com.google.cloud.bigquery.FieldValueList;
import com.google.cloud.bigquery.Job;
import com.google.cloud.bigquery.JobConfiguration;
import com.google.cloud.bigquery.JobException;
import com.google.cloud.bigquery.JobInfo;
import com.google.cloud.bigquery.QueryJobConfiguration;
import com.google.cloud.bigquery.TableResult;
import com.javasampleapproach.bean.Pojo;


public class BigQueryService {

	BigQuery bigquery = BigQueryOptions.newBuilder().setProjectId("helloworld-197309").build().getService();
	Job job = null;
	TableResult result = null;

	public List<Pojo> getUser() throws JobException, InterruptedException{
		String query = "SELECT * FROM myOwnBatabase.babyNames limit 5";
		Pojo pojo ;
		List<Pojo> user =new ArrayList<Pojo>();
		JobConfiguration jobConfiguration = QueryJobConfiguration.of(query);
		JobInfo jobInfo = JobInfo.of(jobConfiguration);
		try {
			job = bigquery.create(jobInfo);
			result = job.getQueryResults();
			System.out.println("result is "+ result);
			for (FieldValueList row : result.iterateAll()) {
				pojo=new Pojo();
				pojo.setName(row.get("name").getStringValue());
				pojo.setGender(row.get("gender").getStringValue());
				pojo.setCount(row.get("count").getLongValue());
				user.add(pojo);
			}


		}
		catch (BigQueryException e) {

		}
		return user;
	}
	public Pojo getUserByCount(long count) throws JobException, InterruptedException {
		String query = "SELECT * FROM myOwnBatabase.babyNames where count ="+ count;
		Pojo pojo = new Pojo() ;
		JobConfiguration jobConfiguration = QueryJobConfiguration.of(query);
		JobInfo jobInfo = JobInfo.of(jobConfiguration);
		try {
			job = bigquery.create(jobInfo);
			result = job.getQueryResults();
			System.out.println("result is "+ result);
			for (FieldValueList row : result.iterateAll()) {

				pojo.setName(row.get("name").getStringValue());
				pojo.setGender(row.get("gender").getStringValue());
				pojo.setCount(row.get("count").getLongValue());

			}


		}
		catch (BigQueryException e) {

		}
		return pojo;

	}
}
