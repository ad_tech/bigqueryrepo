package com.javasampleapproach.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.google.cloud.bigquery.JobException;
import com.javasampleapproach.bean.Pojo;
import com.javasampleapproach.exception.InvalidParameterFoundException;
import com.javasampleapproach.exception.ResourceNotFoundException;
import com.javasampleapproach.service.BigQueryService;


@CrossOrigin
@RestController
//@Api(value="BigQuery" , tags = "BigQuery API", description = "test")
public class bigQueryController {

	@Autowired
	BigQueryService bigQueryService;

	@RequestMapping(value="/getRecord" ,method = RequestMethod.GET)
	//	@ApiOperation(value= "Post method  to get all Polls", notes="Not password protected") // add Apiresponse to customize response code 
	//    @ApiResponses(value= {
	//            @ApiResponse(code=400, message = "Error in calling getPolls"),
	//            @ApiResponse(code=200, message = "Successfully retrieved messages")
	//    })
	public  ResponseEntity<List<Pojo>> getRecord() throws IOException, JobException, InterruptedException{
		List<Pojo> user = bigQueryService.getUser();
		System.out.println(user);

		return new ResponseEntity<List<Pojo>>(user, HttpStatus.OK);

	}

		@RequestMapping(value="/recordById/{count}" ,method = RequestMethod.GET)
		public  Pojo getRecordById(@PathVariable String count) throws IOException, JobException, InterruptedException{
			String s =count;
			long count1 = 0;
			System.out.println("number is "+s);
			for(int i=0;i<s.length()-1;i++) {
				if(!Character.isDigit(s.charAt(i))) {
					throw new InvalidParameterFoundException("Invalid parameter");
				}
				else {
					count1 = Long.parseLong(s);
				}
	
			}
	
	
			Pojo user = bigQueryService.getUserByCount(count1);
			System.out.println(user);
	
			if(user.getName() == null) {
				throw new ResourceNotFoundException(count1, "Resource Not Available");		
			}
	
	
			return user;
	
		}

}


