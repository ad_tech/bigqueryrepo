package com.javasampleapproach.configuration;

import java.util.Arrays;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
//import org.springframework.web.cors.reactive.CorsConfigurationSource;
//import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import com.javasampleapproach.authentication.BigQueryAuthentication;
import com.javasampleapproach.service.BigQueryService;


@Configuration
public class BeanConfiguration {

	
	@Bean
	public static BigQueryAuthentication getBigQueryAuthentication() {
		return new BigQueryAuthentication();
	}
	
	@Bean
	public static BigQueryService getBigQueryService() {
		return new BigQueryService();
	}
//	@Bean
//	   CorsConfigurationSource corsConfigurationSource()
//	   {
//	       CorsConfiguration configuration = new CorsConfiguration();
//	       configuration.setAllowedOrigins(Arrays.asList("https://example.com"));
//	       configuration.setAllowedMethods(Arrays.asList("GET","POST"));
//	       UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
//	       source.registerCorsConfiguration("/**", configuration);
//	       return source;
//	   }
}
