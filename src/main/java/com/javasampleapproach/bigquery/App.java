package com.javasampleapproach.bigquery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.AbstractEnvironment;


@SpringBootApplication
@ComponentScan(basePackages="com.javasampleapproach.*")
public class App 
{	
	
    public static void main(String[] args) throws Exception {
    	System.setProperty(AbstractEnvironment.ACTIVE_PROFILES_PROPERTY_NAME, "QA");
		 SpringApplication.run(App.class, args);	
		 
    }
    
}
