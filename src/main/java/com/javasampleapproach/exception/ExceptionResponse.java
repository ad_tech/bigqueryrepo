package com.javasampleapproach.exception;

import java.util.Date;

public class ExceptionResponse {

   private String status;
   private String detail;
   private String developerMessage;
   private String title;
   private Date timestamp;
   private Error error;
   
   

   public ExceptionResponse() {
   }



public String getStatus() {
	return status;
}



public void setStatus(String status) {
	this.status = status;
}



public String getDetail() {
	return detail;
}



public void setDetail(String detail) {
	this.detail = detail;
}



public String getDeveloperMessage() {
	return developerMessage;
}



public void setDeveloperMessage(String developerMessage) {
	this.developerMessage = developerMessage;
}



public String getTitle() {
	return title;
}



public void setTitle(String title) {
	this.title = title;
}



public Date getTimestamp() {
	return timestamp;
}



public void setTimestamp(Date timestamp) {
	this.timestamp = timestamp;
}



public Error getError() {
	return error;
}



public void setError(Error error) {
	this.error = error;
}





}