package com.javasampleapproach.exception;

public class InvalidParameterFoundException extends RuntimeException{

	public InvalidParameterFoundException(String msg) {
		super(msg);

	}
}
