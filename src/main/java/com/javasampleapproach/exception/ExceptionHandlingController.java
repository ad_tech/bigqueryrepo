package com.javasampleapproach.exception;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

 
//@ControllerAdvice
//public class ExceptionHandlingController {
// 
//	 @ExceptionHandler(MethodArgumentNotValidException.class)
//	    public ResponseEntity<ExceptionResponse> invalidInput(MethodArgumentNotValidException ex) {
//	        ExceptionResponse response = new ExceptionResponse();
//	        response.setErrorCode("Validation Error");
//	        response.setErrorMessage(ex.getMessage());
//	        return new ResponseEntity<ExceptionResponse>(response, HttpStatus.BAD_REQUEST);
//	    
//    }
//}

@ControllerAdvice
public class ExceptionHandlingController {

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<ExceptionResponse> resourceNotFound(ResourceNotFoundException ex) {
        ExceptionResponse response = new ExceptionResponse();
        Error error = new Error();
        error.setMsg1("error1");
        error.setMsg2("error2");
        response.setTitle("Not Found");
        response.setStatus("404");
        response.setDetail(ex.getMessage());
        response.setTimestamp(new Date());
        response.setDeveloperMessage("Resource not found. Please try agin with correct id.");
        response.setError(error);
        return new ResponseEntity<ExceptionResponse>(response, HttpStatus.NOT_FOUND);
    }
    
    @ExceptionHandler(InvalidParameterFoundException.class)
    public ResponseEntity<ExceptionResponse> invalidParameter(InvalidParameterFoundException ex){
    	 ExceptionResponse response = new ExceptionResponse();
         Error error = new Error();
         error.setMsg1("error1");
         error.setMsg2("error2");
         response.setTitle("Invalid Request Parameter");
         response.setStatus("400");
         response.setDetail(ex.getMessage());
         response.setTimestamp(new Date());
         response.setDeveloperMessage("Invalid parameter in requested URL. Provide correct param.");
         response.setError(error);
         return new ResponseEntity<ExceptionResponse>(response, HttpStatus.BAD_REQUEST);
    	
    }
}

