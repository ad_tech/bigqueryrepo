package com.javasampleapproach.authentication;

import java.io.IOException;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.bigquery.Bigquery;
import com.google.api.services.bigquery.BigqueryScopes;

public class BigQueryAuthentication {
	
	  public static Bigquery createAuthorizedClient() throws IOException {
		    // Create the credential
		    HttpTransport transport = new NetHttpTransport();
		    JsonFactory jsonFactory = new JacksonFactory();
		    GoogleCredential credential = GoogleCredential.getApplicationDefault(transport, jsonFactory);

		    if (credential.createScopedRequired()) {
		      credential = credential.createScoped(BigqueryScopes.all());
		    }

		    return new Bigquery.Builder(transport, jsonFactory, credential)
		        .setApplicationName("Bigquery Samples")
		        .build();
		  }
		  
}
